module.exports =  {
    url : 'https://www.joyalukkas.in/',

elements :{
    'CreateUser' : {
            selector : 'a[class=loginusername]'
    },
    'LoginUser' : {
            selector : '//*[@id="myaccount"]/div/div/span[1]',
            locateStrategy : 'xpath'
    },
    'Form' : {
            selector : '.user-login'
    },
    'UsernameField' : {
            selector : '//*[@id="ctl00_ContentPlaceHolder1_ctl00_ctl01_Login1_UserName"]',
            locateStrategy : 'xpath'
    },
    'Password' : {
            selector : '//*[@id="ctl00_ContentPlaceHolder1_ctl00_ctl01_Login1_Password"]',
            locateStrategy : 'xpath'
    },
    'Signin' : {
            selector : '#ctl00_ContentPlaceHolder1_ctl00_ctl01_Login1_LoginImageButton'
    },
    'ErrorMsg' : {
            selector : '//*[@id="ctl00_ContentPlaceHolder1_ctl00_ctl01_Login1_FailureText"]',
            locateStrategy : 'xpath'

    },
    'MetalCategory' : {
            selector : '//*[@id="sample-menu-2"]/li[3]',
            locateStrategy : 'xpath'
    },
    'ClickMorefilter' : {
            selector : '//*[@id="refineAccordion"]/tbody/tr/td/div[6]',
            locateStrategy : 'xpath'
    },
    'SelectSize' : {
            selector : '.refine-more div:nth-child(4)',
    },
    'Sortingdata' : {
            selector : '#ui-accordion-refineAccordion-panel-8',
    },
    'PriceSelecting' : {
            selector : '.prod_viewselectprice'
    },
    'SelectLowToHigh' : {
            selector : '//*[@id="drpSort"]/option[2]',
            locateStrategy : 'xpath'
    },
    'Price' : {
            selector : '.sp_amt'
    },
    'testingprice1' : { 
            selector : '//*[@id="Productshowcase"]/div[1]/div[1]/div[7]/div/div[3]/div[1]/span/label/span',
            locateStrategy : 'xpath'
    }
},


commands : [{
    'checkJoyalukkasPage' : function () {
            return this
            .assert.title('Joyalukkas Online | Joyalukkas Online Shopping Store')
            .assert.visible('body')
    },
    'userLogin' : function () {
         return this
         .click('@CreateUser')
         .click('@LoginUser')
         .assert.visible('@Form')
         .assert.visible('@UsernameField')
         .assert.visible('@Password') 
    },
    'validatingUsernamePassword' : function () {
            return this
            .setValue('@UsernameField','9553456222')
            .setValue('@Password','Sudheer@123')
            .click('@Signin')
    },
    'inValidatingUsernamePassword' : function () {
            return this
            .setValue('@UsernameField','9553456222')
            .setValue('@Password','Sudheer@23')
            .click('@Signin')
    },

    'loginErrorMessage' : function() {
            return this
            .assert.elementPresent('@ErrorMsg')

    },
    'SelectingCategory' : function() {
            return this
            .useXpath().click('@MetalCategory')
    },
    'Filtering' : function() {
            return this
            .useXpath().click('@ClickMorefilter')
            // .click('.prod_viewselectprice')
    },
    'FilteringPrice' : function () {
            return this
            .click('@PriceSelecting')
            // .click('@SelectLowToHigh')
    },
    'PriceSelect' : function () {
            return this
            .useXpath().click('@SelectLowToHigh')
            // .click('@Price')               
    },
    'testingprice' : function () {
            return this
            .useXpath().click('@testingprice1')
    }
}]
};
